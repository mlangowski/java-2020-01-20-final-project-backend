#FROM mysql
#CMD  sudo mysql < script.sql
FROM openjdk:11
ADD target/java-2020-01-20-final-project-backend-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
CMD java -jar java-2020-01-20-final-project-backend-0.0.1-SNAPSHOT.jar --envname=prod

#Build a java jar file first: mvn clean install -Ppmd
#To run java jar file (to make sure it is work): java -jar java-2020-01-20-final-project-backend-0.0.1-SNAPSHOT.jar
#docker build -f Dockerfile -t springboot-game .
#docker run -it -p 8080:8080 springboot-game:latest
