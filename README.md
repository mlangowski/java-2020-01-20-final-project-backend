To run application you can use data from:
1) the built-in h2 database or
2) the mysql server and database from script.sql by building a docker image (you must have the jar file built before).

You can also:
1) use java jar file:
- to build java jar file: 
    mvn clean install -Ppmd
- to run java jar file: 
    java -jar java-2020-01-20-final-project-backend-0.0.1-SNAPSHOT.jar
2) use docker image (you have tou build java jar file first):
- to build docker image: 
    docker build -f Dockerfile -t springboot-game .
- to run docker image: 
    docker run -it -p 8080:8080 springboot-game:latest
