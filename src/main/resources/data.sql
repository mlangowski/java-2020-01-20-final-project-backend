INSERT INTO public.users (id, user_name, password, role, enabled) VALUES
  (1, 'admin', '$2y$12$JDk/7xhIe44wXtCmpthtjOqOl5W3dmmWDXclmIZu9QYtYRW4RNv1u', 'ROLE_ADMIN', true),  -- admin
  (2, 'Adam', '$2y$12$8OcnBwy96mFcK/lzWej94OBx9RdWgZhycrZxvyQqocvHsOkUMyT16', 'ROLE_USER', true),  -- adam
  (3, 'Ewa', '$2y$12$OKl1xJKCA1TxFdpW5mZzVe/UpTVZPMYfjd50U12cNdqAA0juaA72K', 'ROLE_USER', true),  -- ewa
  (4, 'Daniel', '$2y$12$A7dBzdjq7mMO7Cl23LwBWeCJEXtSvA0vyAoFESKXzlF5CpjoD80w6', 'ROLE_USER', true),  -- daniel
  (5, 'Wally', '$2y$12$YI1NLDpjCslOlxomQ3aAqewggLVEKaKJzaa0lsR4UILDc2AwByyhy', 'ROLE_USER', true);  -- wally

INSERT INTO public.resources (id, material, quantity, user_id) VALUES
  (1, 'BASIC', 5, 2),
  (2, 'ADVANCED', 3, 2),
  (3, 'SUPPLY', 1, 2),
  (4, 'BASIC', 5, 3),
  (5, 'ADVANCED', 3, 3),
  (6, 'SUPPLY', 1, 3),
  (7, 'BASIC', 5, 4),
  (8, 'ADVANCED', 3, 4),
  (9, 'SUPPLY', 1, 4),
  (10, 'BASIC', 5, 5),
  (11, 'ADVANCED', 3, 5),
  (12, 'SUPPLY', 1, 5);

INSERT INTO public.units_properties (id, cost_of_basic, cost_of_advanced, cost_of_supply) VALUES
  ('WARRIOR', 1, 0, 1),
  ('ELITE', 0, 1, 2),
  ('LEGENDARY', 1, 2, 5);

INSERT INTO public.units (id, units_properties_id, quantity, user_id) VALUES
  (1, 'WARRIOR', 10, 2),
  (2, 'ELITE', 1, 2),
  (3, 'LEGENDARY', 0, 2),
  (4, 'WARRIOR', 1, 3),
  (5, 'ELITE', 0, 3),
  (6, 'LEGENDARY', 1, 3),
  (7, 'WARRIOR', 3, 4),
  (8, 'ELITE', 0, 4),
  (9, 'LEGENDARY', 0, 4),
  (10, 'WARRIOR', 0, 5),
  (11, 'ELITE', 2, 5),
  (12, 'LEGENDARY', 0, 5);

INSERT INTO public.material_buildings_properties (id, cost_of_basic, cost_of_advanced, generates_material, generates_quantity) VALUES
  ('BASIC_GEN', 2, 0, 'BASIC', 2),
  ('ADVANCED_GEN', 10, 0, 'ADVANCED', 1),
  ('SUPPLY_GEN', 6, 2, 'SUPPLY', 2);

INSERT INTO public.material_buildings (id, material_buildings_properties_id, user_id) VALUES
  (1, 'BASIC_GEN', 2),
  (2, 'ADVANCED_GEN', 2),
  (3, 'BASIC_GEN', 3),
  (4, 'ADVANCED_GEN', 3),
  (5, 'BASIC_GEN', 4),
  (6, 'ADVANCED_GEN', 4),
  (7, 'BASIC_GEN', 5),
  (8, 'SUPPLY_GEN', 5);

INSERT INTO public.army_buildings_properties (id, cost_of_basic, cost_of_advanced) VALUES
  ('WARRIOR_GEN', 2, 0),
  ('ELITE_GEN', 2, 1),
  ('LEGENDARY_GEN', 2, 3);

INSERT INTO public.army_buildings (id, army_buildings_properties_id, user_id) VALUES
  (1, 'WARRIOR_GEN', 2),
  (2, 'WARRIOR_GEN', 3),
  (3, 'ELITE_GEN', 4);
