package cm.third.project.group.java20200120finalprojectbackend.controller;

import cm.third.project.group.java20200120finalprojectbackend.dto.UserDto;
import cm.third.project.group.java20200120finalprojectbackend.entity.*;
import cm.third.project.group.java20200120finalprojectbackend.repository.*;
import cm.third.project.group.java20200120finalprojectbackend.service.UserService;
import cm.third.project.group.java20200120finalprojectbackend.service.UserToDtoAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserRepository userRepository;
    private final UserToDtoAdapter userToDtoAdapter;
    private final UserService userService;

    @Autowired
    public UserController(UserRepository userRepository, UserToDtoAdapter userToDtoAdapter, UserService userService) {
        this.userRepository = userRepository;
        this.userToDtoAdapter = userToDtoAdapter;
        this.userService = userService;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> findUsers() {
        List<User> users = userRepository.findAll();
        List<UserDto> usersDto;
        usersDto = users.stream().map(userToDtoAdapter::getUserDto).collect(Collectors.toList());

        return new ResponseEntity<>(usersDto, HttpStatus.OK);
    }

    @GetMapping(value = "/user", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> findUser() {
        Optional<UserDto> userDto = userService.getActualUser();
        if (userDto.isPresent()) {
            return new ResponseEntity<>(userDto, HttpStatus.OK);
        }
        return new ResponseEntity<>(Optional.empty(), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> findUserById(@PathVariable("id") Integer id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            return new ResponseEntity<>(Optional.empty(), HttpStatus.NOT_FOUND);
        }
        if (userService.isAuth(id)) {
            UserDto userDto = userToDtoAdapter.getUserDto(user.get());
            return new ResponseEntity<>(userDto, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> postUser(@RequestBody User user, UriComponentsBuilder uri,
                                           BindingResult bindingResult) {

        if (!bindingResult.hasErrors()) {
            userService.addUser(user);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(uri.path("users/{id}").buildAndExpand(user.getId()).toUri());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable("id") Integer id) {

        if (userService.isAuth(id) || userService.isAdmin()) {
            Optional<User> user = userRepository.findById(id);
            if (user.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            userRepository.delete(user.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
