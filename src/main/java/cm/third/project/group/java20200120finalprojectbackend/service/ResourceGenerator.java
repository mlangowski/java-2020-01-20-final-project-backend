package cm.third.project.group.java20200120finalprojectbackend.service;

import cm.third.project.group.java20200120finalprojectbackend.entity.Materials;
import cm.third.project.group.java20200120finalprojectbackend.entity.Resource;
import cm.third.project.group.java20200120finalprojectbackend.entity.Unit;
import cm.third.project.group.java20200120finalprojectbackend.entity.User;
import cm.third.project.group.java20200120finalprojectbackend.repository.MaterialBuildingRepository;
import cm.third.project.group.java20200120finalprojectbackend.repository.ResourceRepository;
import cm.third.project.group.java20200120finalprojectbackend.repository.UnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResourceGenerator {

    private final MaterialBuildingRepository materialBuildingRepository;
    private final ResourceRepository resourceRepository;
    private final UnitRepository unitRepository;

    @Autowired
    public ResourceGenerator(MaterialBuildingRepository materialBuildingRepository,
                             ResourceRepository resourceRepository, UnitRepository unitRepository) {
        this.materialBuildingRepository = materialBuildingRepository;
        this.resourceRepository = resourceRepository;
        this.unitRepository = unitRepository;
    }

    public void generateResources() {
        System.out.println(
                "Fixed delay task - " + System.currentTimeMillis() / 10000);

        for (int i = 1; i <= materialBuildingRepository.findAll().size(); i++) {
            if (materialBuildingRepository.findById(i).isPresent()) {

                User user = materialBuildingRepository.findById(i).get().getUser();

                Materials material = materialBuildingRepository
                        .findById(i).get().getMaterialBuildingsProperties().getGeneratesMaterial();

                int generatesQuantity = materialBuildingRepository
                        .findById(i).get().getMaterialBuildingsProperties().getGeneratesQuantity();

                Resource resource = resourceRepository.findByUserAndMaterial(user, material);

                resource.setQuantity(resource.getQuantity() + generatesQuantity);

                resourceRepository.save(resource);
            }
        }

        for (int i = 1; i <= unitRepository.findAll().size(); i++) {
            if (unitRepository.findById(i).isPresent()) {
                User user = unitRepository.findById(i).get().getUser();
                int unitsQuantity = unitRepository.findById(i).get().getQuantity();
                int supplyConsumption = unitRepository.findById(i).get().getUnitsProperties().getCostOfSupply();
                Resource resource = resourceRepository.findByUserAndMaterial(user, Materials.SUPPLY);
                resource.setQuantity(resource.getQuantity() - (unitsQuantity * supplyConsumption));

                if (resource.getQuantity() < 0) {
                    Unit unit = unitRepository.findById(i).get();
                    if (unit.getQuantity() > 0) {
                        unit.setQuantity(unit.getQuantity() - 1);
                        unitRepository.save(unit);
                    }
                    resource.setQuantity(0);
                }
                resourceRepository.save(resource);
            }
        }
    }
}
