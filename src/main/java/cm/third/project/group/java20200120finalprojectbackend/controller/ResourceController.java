package cm.third.project.group.java20200120finalprojectbackend.controller;

import cm.third.project.group.java20200120finalprojectbackend.dto.UserDto;
import cm.third.project.group.java20200120finalprojectbackend.entity.User;
import cm.third.project.group.java20200120finalprojectbackend.repository.UserRepository;
import cm.third.project.group.java20200120finalprojectbackend.service.ResourceGenerator;
import cm.third.project.group.java20200120finalprojectbackend.service.UserService;
import cm.third.project.group.java20200120finalprojectbackend.service.UserToDtoAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@Controller
@RestController
@RequestMapping("/api/users")
public class ResourceController {

    private final ResourceGenerator resourceGenerator;
    private final UserRepository userRepository;
    private final UserService userService;
    private final UserToDtoAdapter userToDtoAdapter;

    @Autowired
    public ResourceController(ResourceGenerator resourceGenerator, UserRepository userRepository,
                              UserService userService, UserToDtoAdapter userToDtoAdapter) {
        this.resourceGenerator = resourceGenerator;
        this.userRepository = userRepository;
        this.userService = userService;
        this.userToDtoAdapter = userToDtoAdapter;
    }

    @Scheduled(fixedDelay = 10000)
    public void scheduleFixedDelayTask() {
        resourceGenerator.generateResources();
    }

    @GetMapping("/{id}/resources")
    public ResponseEntity<Object> findResourcesByUserId(@PathVariable("id") Integer id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            return new ResponseEntity<>(Optional.empty(), HttpStatus.NOT_FOUND);
        }
        if (userService.isAuth(id)) {
            UserDto userDto = userToDtoAdapter.getUserDto(user.get());
            return new ResponseEntity<>(userDto.getResources(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
