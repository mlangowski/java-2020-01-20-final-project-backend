package cm.third.project.group.java20200120finalprojectbackend.repository;

import cm.third.project.group.java20200120finalprojectbackend.entity.ArmyBuildings;
import cm.third.project.group.java20200120finalprojectbackend.entity.ArmyBuildingsProperties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArmyBuildingsPropertiesRepository extends JpaRepository<ArmyBuildingsProperties, ArmyBuildings> {
}
