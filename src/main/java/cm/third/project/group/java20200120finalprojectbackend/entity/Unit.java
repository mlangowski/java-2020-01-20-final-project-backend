package cm.third.project.group.java20200120finalprojectbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "units")
public class Unit {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private int quantity;

    @ManyToOne
    @JsonIgnore
    private User user;

    @ManyToOne
    @JoinColumn
    private UnitsProperties unitsProperties;

    public Unit() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UnitsProperties getUnitsProperties() {
        return unitsProperties;
    }

    public void setUnitsProperties(UnitsProperties unitsProperties) {
        this.unitsProperties = unitsProperties;
    }
}
