package cm.third.project.group.java20200120finalprojectbackend.repository;

import cm.third.project.group.java20200120finalprojectbackend.entity.Unit;
import cm.third.project.group.java20200120finalprojectbackend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UnitRepository extends JpaRepository<Unit, Integer> {

    @Transactional
    Optional<Unit> findByUserAndId(User user, Integer id);

}
