package cm.third.project.group.java20200120finalprojectbackend.service;

import cm.third.project.group.java20200120finalprojectbackend.entity.*;
import cm.third.project.group.java20200120finalprojectbackend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArmyBuildingService {

    private final ResourceRepository resourceRepository;
    private final ArmyBuildingRepository armyBuildingRepository;
    private final ArmyBuildingsPropertiesRepository armyBuildingsPropertiesRepository;

    @Autowired
    public ArmyBuildingService(ResourceRepository resourceRepository, ArmyBuildingRepository armyBuildingRepository,
                               ArmyBuildingsPropertiesRepository armyBuildingsPropertiesRepository) {

        this.resourceRepository = resourceRepository;
        this.armyBuildingRepository = armyBuildingRepository;
        this.armyBuildingsPropertiesRepository = armyBuildingsPropertiesRepository;
    }

    public boolean isArmyBuildingBuilt(User user, ArmyBuilding armyBuilding) {

        ArmyBuildings buildingType = armyBuilding.getArmyBuildingsProperties().getId();

        if (isArmyBuildingExists(user, buildingType)) {
            return false;
        }

        switch (buildingType) {
            case ELITE_GEN:
                if (!isArmyBuildingExists(user, ArmyBuildings.WARRIOR_GEN)) {
                    return false;
                }
                break;
            case LEGENDARY_GEN:
                if (!isArmyBuildingExists(user, ArmyBuildings.ELITE_GEN)) {
                    return false;
                }
        }

        if (armyBuildingsPropertiesRepository.findById(buildingType).isPresent()) {
            int quantityBasicToBuild = armyBuildingsPropertiesRepository
                    .findById(buildingType).get().getCostOfBasic();
            int quantityAdvancedToBuild = armyBuildingsPropertiesRepository
                    .findById(buildingType).get().getCostOfAdvanced();

            if (quantityBasicToBuild <= user.getResources().get(0).getQuantity()
                    && quantityAdvancedToBuild <= user.getResources().get(1).getQuantity()) {

                Resource basic = resourceRepository.findByUserAndMaterial(user, Materials.BASIC);
                Resource advanced = resourceRepository.findByUserAndMaterial(user, Materials.ADVANCED);
                basic.setQuantity(basic.getQuantity() - quantityBasicToBuild);
                advanced.setQuantity(advanced.getQuantity() - quantityAdvancedToBuild);
                resourceRepository.save(basic);
                resourceRepository.save(advanced);

                armyBuilding.setUser(user);
                armyBuildingRepository.save(armyBuilding);

                return true;
            }
        }
        return false;
    }

    private boolean isArmyBuildingExists(User user, ArmyBuildings armyBuilding) {

        List<ArmyBuilding> armyBuildings = user.getArmyBuildings();

        for (ArmyBuilding existArmyBuilding : armyBuildings) {
            if (armyBuilding.equals(existArmyBuilding.getArmyBuildingsProperties().getId())) {
                return true;
            }
        }
        return false;
    }
}
