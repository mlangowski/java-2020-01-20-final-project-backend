package cm.third.project.group.java20200120finalprojectbackend.service;

import cm.third.project.group.java20200120finalprojectbackend.dto.UnitDto;
import cm.third.project.group.java20200120finalprojectbackend.dto.UnitDtoBuilder;

import cm.third.project.group.java20200120finalprojectbackend.entity.Unit;
import org.springframework.stereotype.Service;

@Service
public class UnitToDtoAdapter {

    public UnitToDtoAdapter() {
    }

    public UnitDto getUnitDto(Unit unit) {
        return new UnitDtoBuilder()
                .withId(unit.getId())
                .withQuantity(unit.getQuantity())
                .withUnitsProperties(unit.getUnitsProperties())
                .build();
    }
}
