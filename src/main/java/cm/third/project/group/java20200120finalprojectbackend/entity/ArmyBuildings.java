package cm.third.project.group.java20200120finalprojectbackend.entity;

public enum ArmyBuildings {

    WARRIOR_GEN,
    ELITE_GEN,
    LEGENDARY_GEN
}
