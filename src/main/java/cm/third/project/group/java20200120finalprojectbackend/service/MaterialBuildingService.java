package cm.third.project.group.java20200120finalprojectbackend.service;

import cm.third.project.group.java20200120finalprojectbackend.entity.*;
import cm.third.project.group.java20200120finalprojectbackend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MaterialBuildingService {

    private final ResourceRepository resourceRepository;
    private final MaterialBuildingRepository materialBuildingRepository;
    private final MaterialBuildingsPropertiesRepository materialBuildingPropertiesRepository;

    @Autowired
    public MaterialBuildingService(ResourceRepository resourceRepository, MaterialBuildingRepository materialBuildingRepository,
                                   MaterialBuildingsPropertiesRepository materialBuildingPropertiesRepository) {

        this.resourceRepository = resourceRepository;
        this.materialBuildingRepository = materialBuildingRepository;
        this.materialBuildingPropertiesRepository = materialBuildingPropertiesRepository;
    }

    public boolean isMaterialBuildingBuilt(User user, MaterialBuilding materialBuilding) {

        MaterialBuildings buildingType = materialBuilding.getMaterialBuildingsProperties().getId();

        if (materialBuildingPropertiesRepository.findById(buildingType).isPresent()) {
            int quantityBasicToBuild = materialBuildingPropertiesRepository
                    .findById(buildingType).get().getCostOfBasic();
            int quantityAdvancedToBuild = materialBuildingPropertiesRepository
                    .findById(buildingType).get().getCostOfAdvanced();

            if (quantityBasicToBuild <= user.getResources().get(0).getQuantity()
                    && quantityAdvancedToBuild <= user.getResources().get(1).getQuantity()) {

                Resource basic = resourceRepository.findByUserAndMaterial(user, Materials.BASIC);
                Resource advanced = resourceRepository.findByUserAndMaterial(user, Materials.ADVANCED);
                basic.setQuantity(basic.getQuantity() - quantityBasicToBuild);
                advanced.setQuantity(advanced.getQuantity() - quantityAdvancedToBuild);
                resourceRepository.save(basic);
                resourceRepository.save(advanced);

                materialBuilding.setUser(user);
                materialBuildingRepository.save(materialBuilding);

                return true;
            }
        }
        return false;
    }
}
