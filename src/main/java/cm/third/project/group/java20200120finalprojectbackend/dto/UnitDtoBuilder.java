package cm.third.project.group.java20200120finalprojectbackend.dto;

import cm.third.project.group.java20200120finalprojectbackend.entity.UnitsProperties;

public class UnitDtoBuilder {

    private Integer id;

    private int quantity;

    private UnitsProperties unitsProperties;

    public UnitDtoBuilder() {
    }

    public UnitDtoBuilder(Integer id, int quantity, UnitsProperties unitsProperties) {
        this.id = id;
        this.quantity = quantity;
        this.unitsProperties = unitsProperties;
    }

    public UnitDtoBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public UnitDtoBuilder withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public UnitDtoBuilder withUnitsProperties(UnitsProperties unitsProperties) {
        this.unitsProperties = unitsProperties;
        return this;
    }

    public UnitDto build() {
        UnitDto unitDto = new UnitDto();
        unitDto.setId(id);
        unitDto.setQuantity(quantity);
        unitDto.setUnitsProperties(unitsProperties);
        return unitDto;
    }
}
