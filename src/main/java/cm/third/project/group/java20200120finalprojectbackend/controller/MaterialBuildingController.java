package cm.third.project.group.java20200120finalprojectbackend.controller;

import cm.third.project.group.java20200120finalprojectbackend.dto.UserDto;
import cm.third.project.group.java20200120finalprojectbackend.entity.MaterialBuilding;
import cm.third.project.group.java20200120finalprojectbackend.entity.User;
import cm.third.project.group.java20200120finalprojectbackend.repository.UserRepository;
import cm.third.project.group.java20200120finalprojectbackend.service.MaterialBuildingService;
import cm.third.project.group.java20200120finalprojectbackend.service.UserService;
import cm.third.project.group.java20200120finalprojectbackend.service.UserToDtoAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class MaterialBuildingController {

    private final UserRepository userRepository;
    private final UserToDtoAdapter userToDtoAdapter;
    private final UserService userService;
    private final MaterialBuildingService materialBuildingService;

    @Autowired
    public MaterialBuildingController(UserRepository userRepository, UserToDtoAdapter userToDtoAdapter,
                                      UserService userService, MaterialBuildingService materialBuildingService) {
        this.userRepository = userRepository;
        this.userToDtoAdapter = userToDtoAdapter;
        this.userService = userService;
        this.materialBuildingService = materialBuildingService;
    }

    @GetMapping("/{id}/material_buildings")
    public ResponseEntity<Object> findMaterialBuildingsByUserId(@PathVariable("id") Integer id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            return new ResponseEntity<>(Optional.empty(), HttpStatus.NOT_FOUND);
        }
        if (userService.isAuth(id)) {
            UserDto userDto = userToDtoAdapter.getUserDto(user.get());
            return new ResponseEntity<>(userDto.getMaterialBuildings(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PostMapping(value = "/{id}/material_buildings", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> postMaterialBuilding(@PathVariable("id") Integer id,
                                                       @RequestBody MaterialBuilding materialBuilding,
                                                       UriComponentsBuilder uri, BindingResult bindingResult) {

        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            return new ResponseEntity<>(Optional.empty(), HttpStatus.NOT_FOUND);
        }

        if (userService.isAuth(id)) {
            if (!bindingResult.hasErrors()) {

                if (materialBuildingService.isMaterialBuildingBuilt(user.get(), materialBuilding)) {

                    HttpHeaders headers = new HttpHeaders();
                    headers.setLocation(uri.path("users/{id}/material_buildings/{mbId}").buildAndExpand(id, materialBuilding.getId()).toUri());
                    return new ResponseEntity<>(headers, HttpStatus.CREATED);
                }
                return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
            }
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
