package cm.third.project.group.java20200120finalprojectbackend.entity;

import javax.persistence.*;

@Entity
@Table(name = "army_buildings_properties")
public class ArmyBuildingsProperties {

    @Id
    @Enumerated(EnumType.STRING)
    private ArmyBuildings id;

    private int costOfBasic;

    private int costOfAdvanced;

    public ArmyBuildingsProperties() {
    }

    public ArmyBuildings getId() {
        return id;
    }

    public void setId(ArmyBuildings id) {
        this.id = id;
    }

    public int getCostOfBasic() {
        return costOfBasic;
    }

    public void setCostOfBasic(int costOfBasic) {
        this.costOfBasic = costOfBasic;
    }

    public int getCostOfAdvanced() {
        return costOfAdvanced;
    }

    public void setCostOfAdvanced(int costOfAdvanced) {
        this.costOfAdvanced = costOfAdvanced;
    }

}
