package cm.third.project.group.java20200120finalprojectbackend.controller;

import cm.third.project.group.java20200120finalprojectbackend.dto.UnitDto;
import cm.third.project.group.java20200120finalprojectbackend.entity.Unit;
import cm.third.project.group.java20200120finalprojectbackend.entity.User;
import cm.third.project.group.java20200120finalprojectbackend.repository.UserRepository;
import cm.third.project.group.java20200120finalprojectbackend.service.UnitService;
import cm.third.project.group.java20200120finalprojectbackend.service.UnitToDtoAdapter;
import cm.third.project.group.java20200120finalprojectbackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UnitController {

    private final UserRepository userRepository;
    private final UnitToDtoAdapter unitToDtoAdapter;
    private final UserService userService;
    private final UnitService unitService;

    @Autowired
    public UnitController(UserRepository userRepository, UnitToDtoAdapter unitToDtoAdapter,
                          UserService userService, UnitService unitService) {

        this.userRepository = userRepository;
        this.unitToDtoAdapter = unitToDtoAdapter;
        this.userService = userService;
        this.unitService = unitService;
    }

    @GetMapping("/{id}/units")
    public ResponseEntity<Object> findUnitsByUserId(@PathVariable("id") Integer id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty() || user.get().getUnits().isEmpty()) {
            return new ResponseEntity<>(Optional.empty(), HttpStatus.NOT_FOUND);
        }
        List<UnitDto> unitsDto;
        unitsDto = user.get().getUnits().stream().map(unitToDtoAdapter::getUnitDto).collect(Collectors.toList());
        if (userService.isAuth(id)) {
            return new ResponseEntity<>(unitsDto, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PutMapping(value = "/{id}/units") // /{uId}
    public ResponseEntity<Object> putUnit(@PathVariable("id") Integer id, //@PathVariable("uId") Integer uId,
                                          @RequestBody Unit unit, BindingResult bindingResult) {

        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            return new ResponseEntity<>(Optional.empty(), HttpStatus.NOT_FOUND);
        }
        if (userService.isAuth(id)) {
            if (!bindingResult.hasErrors()) {
                if (unitService.isUnitHired(user.get(), unit)) {
                    return new ResponseEntity<>(HttpStatus.ACCEPTED);
                }
                return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
            }
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
