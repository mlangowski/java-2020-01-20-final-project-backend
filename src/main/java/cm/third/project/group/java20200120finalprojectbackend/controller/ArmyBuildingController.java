package cm.third.project.group.java20200120finalprojectbackend.controller;

import cm.third.project.group.java20200120finalprojectbackend.dto.UserDto;
import cm.third.project.group.java20200120finalprojectbackend.entity.ArmyBuilding;
import cm.third.project.group.java20200120finalprojectbackend.entity.User;
import cm.third.project.group.java20200120finalprojectbackend.repository.UserRepository;
import cm.third.project.group.java20200120finalprojectbackend.service.ArmyBuildingService;
import cm.third.project.group.java20200120finalprojectbackend.service.UserService;
import cm.third.project.group.java20200120finalprojectbackend.service.UserToDtoAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class ArmyBuildingController {

    private final UserRepository userRepository;
    private final UserToDtoAdapter userToDtoAdapter;
    private final UserService userService;
    private final ArmyBuildingService armyBuildingService;

    @Autowired
    public ArmyBuildingController(UserRepository userRepository, UserToDtoAdapter userToDtoAdapter,
                                  UserService userService, ArmyBuildingService armyBuildingService) {
        this.userRepository = userRepository;
        this.userToDtoAdapter = userToDtoAdapter;
        this.userService = userService;
        this.armyBuildingService = armyBuildingService;
    }

    @GetMapping("/{id}/army_buildings")
    public ResponseEntity<Object> findArmyBuildingsByUserId(@PathVariable("id") Integer id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            return new ResponseEntity<>(Optional.empty(), HttpStatus.NOT_FOUND);
        }
        if (userService.isAuth(id)) {
            UserDto userDto = userToDtoAdapter.getUserDto(user.get());
            return new ResponseEntity<>(userDto.getArmyBuildings(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PostMapping(value = "/{id}/army_buildings", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> postArmyBuilding(@PathVariable("id") Integer id, @RequestBody ArmyBuilding armyBuilding,
                                                   UriComponentsBuilder uri, BindingResult bindingResult) {

        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            return new ResponseEntity<>(Optional.empty(), HttpStatus.NOT_FOUND);
        }
        if (userService.isAuth(id)) {
            if (!bindingResult.hasErrors()) {

                if (armyBuildingService.isArmyBuildingBuilt(user.get(), armyBuilding)) {

                    HttpHeaders headers = new HttpHeaders();
                    headers.setLocation(uri.path("users/{id}/army_buildings/{abId}").buildAndExpand(id, armyBuilding.getId()).toUri());
                    return new ResponseEntity<>(headers, HttpStatus.CREATED);
                }
                return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
            }
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
