package cm.third.project.group.java20200120finalprojectbackend.dto;

import cm.third.project.group.java20200120finalprojectbackend.entity.UnitsProperties;

public class UnitDto {

    private Integer id;

    private int quantity;

    private UnitsProperties unitsProperties;

    public UnitDto() {
    }

    public UnitDto(Integer id, int quantity, UnitsProperties unitsProperties) {
        this.id = id;
        this.quantity = quantity;
        this.unitsProperties = unitsProperties;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public UnitsProperties getUnitsProperties() {
        return unitsProperties;
    }

    public void setUnitsProperties(UnitsProperties unitsProperties) {
        this.unitsProperties = unitsProperties;
    }
}
