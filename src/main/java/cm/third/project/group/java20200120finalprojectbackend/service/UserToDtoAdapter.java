package cm.third.project.group.java20200120finalprojectbackend.service;

import cm.third.project.group.java20200120finalprojectbackend.dto.UserDto;
import cm.third.project.group.java20200120finalprojectbackend.dto.UserDtoBuilder;
import cm.third.project.group.java20200120finalprojectbackend.entity.User;
import org.springframework.stereotype.Service;

@Service
public class UserToDtoAdapter {

    public UserToDtoAdapter() {
    }

    public UserDto getUserDto(User user) {
        return new UserDtoBuilder()
                .withId(user.getId())
                .withUserName(user.getUserName())
                .withArmyBuildings(user.getArmyBuildings())
                .withMaterialBuildings(user.getMaterialBuildings())
                .withResources(user.getResources())
                .withUnits(user.getUnits())
                .build();
    }
}
