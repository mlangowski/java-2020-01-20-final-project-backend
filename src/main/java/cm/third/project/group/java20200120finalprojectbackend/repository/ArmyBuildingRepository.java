package cm.third.project.group.java20200120finalprojectbackend.repository;

import cm.third.project.group.java20200120finalprojectbackend.entity.ArmyBuilding;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArmyBuildingRepository extends JpaRepository<ArmyBuilding, Integer> {

}
