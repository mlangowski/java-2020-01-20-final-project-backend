package cm.third.project.group.java20200120finalprojectbackend.dto;

import cm.third.project.group.java20200120finalprojectbackend.entity.ArmyBuilding;
import cm.third.project.group.java20200120finalprojectbackend.entity.MaterialBuilding;
import cm.third.project.group.java20200120finalprojectbackend.entity.Resource;
import cm.third.project.group.java20200120finalprojectbackend.entity.Unit;

import java.util.ArrayList;
import java.util.List;

public class UserDtoBuilder {

    private Integer id;

    private String userName;

    private List<MaterialBuilding> materialBuildings = new ArrayList<>();

    private List<ArmyBuilding> armyBuildings = new ArrayList<>();

    private List<Unit> units = new ArrayList<>();

    private List<Resource> resources = new ArrayList<>();

    public UserDtoBuilder() {
    }

    public UserDtoBuilder(Integer id, String userName, List<MaterialBuilding> materialBuildings,
                          List<ArmyBuilding> armyBuildings, List<Unit> units, List<Resource> resources) {
        this.id = id;
        this.userName = userName;
        this.materialBuildings = materialBuildings;
        this.armyBuildings = armyBuildings;
        this.units = units;
        this.resources = resources;
    }

    public UserDtoBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public UserDtoBuilder withUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public UserDtoBuilder withMaterialBuildings(List<MaterialBuilding> materialBuildings) {
        this.materialBuildings = materialBuildings;
        return this;
    }

    public UserDtoBuilder withArmyBuildings(List<ArmyBuilding> armyBuildings) {
        this.armyBuildings = armyBuildings;
        return this;
    }

    public UserDtoBuilder withUnits(List<Unit> units) {
        this.units = units;
        return this;
    }

    public UserDtoBuilder withResources(List<Resource> resources) {
        this.resources = resources;
        return this;
    }

    public UserDto build() {
        UserDto userDto = new UserDto();
        userDto.setId(id);
        userDto.setUserName(userName);
        userDto.setArmyBuildings(armyBuildings);
        userDto.setMaterialBuildings(materialBuildings);
        userDto.setResources(resources);
        userDto.setUnits(units);
        return userDto;
    }
}
