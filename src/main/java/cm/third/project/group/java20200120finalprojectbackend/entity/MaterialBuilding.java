package cm.third.project.group.java20200120finalprojectbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "material_buildings")
public class MaterialBuilding {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @ManyToOne
    @JsonIgnore
    private User user;

    @ManyToOne
    @JoinColumn
    private MaterialBuildingsProperties materialBuildingsProperties;

    public MaterialBuilding() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public MaterialBuildingsProperties getMaterialBuildingsProperties() {
        return materialBuildingsProperties;
    }

    public void setMaterialBuildingsProperties(MaterialBuildingsProperties materialBuildingsProperties) {
        this.materialBuildingsProperties = materialBuildingsProperties;
    }
}
