package cm.third.project.group.java20200120finalprojectbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "army_buildings")
public class ArmyBuilding {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @ManyToOne
    @JsonIgnore
    private User user;

    @ManyToOne
    @JoinColumn
    private ArmyBuildingsProperties armyBuildingsProperties;

    public ArmyBuilding() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArmyBuildingsProperties getArmyBuildingsProperties() {
        return armyBuildingsProperties;
    }

    public void setArmyBuildingsProperties(ArmyBuildingsProperties armyBuildingsProperties) {
        this.armyBuildingsProperties = armyBuildingsProperties;
    }
}
