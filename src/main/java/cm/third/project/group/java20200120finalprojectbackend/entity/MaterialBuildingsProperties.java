package cm.third.project.group.java20200120finalprojectbackend.entity;

import javax.persistence.*;

@Entity
@Table(name = "material_buildings_properties")
public class MaterialBuildingsProperties {

    @Id
    @Enumerated(EnumType.STRING)
    private MaterialBuildings id;

    private int costOfBasic;

    private int costOfAdvanced;

    @Enumerated(EnumType.STRING)
    private Materials generatesMaterial;

    private int generatesQuantity;

    public MaterialBuildingsProperties() {
    }

    public MaterialBuildings getId() {
        return id;
    }

    public void setId(MaterialBuildings id) {
        this.id = id;
    }

    public int getCostOfBasic() {
        return costOfBasic;
    }

    public void setCostOfBasic(int costOfBasic) {
        this.costOfBasic = costOfBasic;
    }

    public int getCostOfAdvanced() {
        return costOfAdvanced;
    }

    public void setCostOfAdvanced(int costOfAdvanced) {
        this.costOfAdvanced = costOfAdvanced;
    }

    public Materials getGeneratesMaterial() {
        return generatesMaterial;
    }

    public void setGeneratesMaterial(Materials generatesMaterial) {
        this.generatesMaterial = generatesMaterial;
    }

    public int getGeneratesQuantity() {
        return generatesQuantity;
    }

    public void setGeneratesQuantity(int generatesQuantity) {
        this.generatesQuantity = generatesQuantity;
    }

}
