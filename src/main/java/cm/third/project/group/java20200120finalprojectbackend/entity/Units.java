package cm.third.project.group.java20200120finalprojectbackend.entity;

public enum Units {

    WARRIOR,
    ELITE,
    LEGENDARY
}
