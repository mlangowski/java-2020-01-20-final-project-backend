package cm.third.project.group.java20200120finalprojectbackend.dto;

import cm.third.project.group.java20200120finalprojectbackend.entity.ArmyBuilding;
import cm.third.project.group.java20200120finalprojectbackend.entity.MaterialBuilding;
import cm.third.project.group.java20200120finalprojectbackend.entity.Resource;
import cm.third.project.group.java20200120finalprojectbackend.entity.Unit;

import java.util.ArrayList;
import java.util.List;

public class UserDto {

    private Integer id;

    private String userName;

    private List<MaterialBuilding> materialBuildings = new ArrayList<>();

    private List<ArmyBuilding> armyBuildings = new ArrayList<>();

    private List<Unit> units = new ArrayList<>();

    private List<Resource> resources = new ArrayList<>();

    public UserDto() {
    }

    public UserDto(Integer id, String userName, List<MaterialBuilding> materialBuildings,
                   List<ArmyBuilding> armyBuildings, List<Unit> units, List<Resource> resources) {
        this.id = id;
        this.userName = userName;
        this.materialBuildings = materialBuildings;
        this.armyBuildings = armyBuildings;
        this.units = units;
        this.resources = resources;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<MaterialBuilding> getMaterialBuildings() {
        return materialBuildings;
    }

    public void setMaterialBuildings(List<MaterialBuilding> materialBuildings) {
        this.materialBuildings = materialBuildings;
    }

    public List<ArmyBuilding> getArmyBuildings() {
        return armyBuildings;
    }

    public void setArmyBuildings(List<ArmyBuilding> armyBuildings) {
        this.armyBuildings = armyBuildings;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public void setUnits(List<Unit> units) {
        this.units = units;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }
}
