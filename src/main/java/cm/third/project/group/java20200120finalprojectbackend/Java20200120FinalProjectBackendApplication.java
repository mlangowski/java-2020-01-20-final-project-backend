package cm.third.project.group.java20200120finalprojectbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Java20200120FinalProjectBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(Java20200120FinalProjectBackendApplication.class, args);
    }

}
