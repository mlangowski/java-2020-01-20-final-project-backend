package cm.third.project.group.java20200120finalprojectbackend.entity;

import javax.persistence.*;

@Entity
@Table(name = "units_properties")
public class UnitsProperties {

    @Id
    @Enumerated(EnumType.STRING)
    private Units id;

    private int costOfBasic;

    private int costOfAdvanced;

    private int costOfSupply;

    public UnitsProperties() {
    }

    public Units getId() {
        return id;
    }

    public void setId(Units id) {
        this.id = id;
    }

    public int getCostOfBasic() {
        return costOfBasic;
    }

    public void setCostOfBasic(int costOfBasic) {
        this.costOfBasic = costOfBasic;
    }

    public int getCostOfAdvanced() {
        return costOfAdvanced;
    }

    public void setCostOfAdvanced(int costOfAdvanced) {
        this.costOfAdvanced = costOfAdvanced;
    }

    public int getCostOfSupply() {
        return costOfSupply;
    }

    public void setCostOfSupply(int costOfSupply) {
        this.costOfSupply = costOfSupply;
    }
}
