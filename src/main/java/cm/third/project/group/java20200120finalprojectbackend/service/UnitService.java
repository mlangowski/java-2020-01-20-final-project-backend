package cm.third.project.group.java20200120finalprojectbackend.service;

import cm.third.project.group.java20200120finalprojectbackend.entity.Resource;
import cm.third.project.group.java20200120finalprojectbackend.entity.Unit;
import cm.third.project.group.java20200120finalprojectbackend.entity.User;
import cm.third.project.group.java20200120finalprojectbackend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnitService {

    private final ResourceRepository resourceRepository;
    private final UnitRepository unitRepository;

    @Autowired
    public UnitService(ResourceRepository resourceRepository, UnitRepository unitRepository) {

        this.resourceRepository = resourceRepository;
        this.unitRepository = unitRepository;
    }

    public boolean isUnitHired(User user, Unit unit) {

        if (unitRepository.findById(unit.getId()).isPresent()) {
            Unit existingUnit = unitRepository.findById(unit.getId()).get();

            int quantityBasicToHire = existingUnit.getUnitsProperties().getCostOfBasic() * unit.getQuantity();
            int quantityAdvancedToHire = existingUnit.getUnitsProperties().getCostOfAdvanced() * unit.getQuantity();

            Resource basic = user.getResources().get(0);
            Resource advanced = user.getResources().get(1);

            if (basic.getQuantity() >= quantityBasicToHire && advanced.getQuantity() >= quantityAdvancedToHire) {
                basic.setQuantity(basic.getQuantity() - quantityBasicToHire);
                advanced.setQuantity(advanced.getQuantity() - quantityAdvancedToHire);
                existingUnit.setQuantity(existingUnit.getQuantity() + unit.getQuantity());
                resourceRepository.save(basic);
                resourceRepository.save(advanced);
                unitRepository.save(existingUnit);
                return true;
            }
        }
        return false;
    }
}
