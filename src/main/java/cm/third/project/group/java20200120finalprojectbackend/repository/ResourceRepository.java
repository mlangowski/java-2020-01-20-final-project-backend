package cm.third.project.group.java20200120finalprojectbackend.repository;

import cm.third.project.group.java20200120finalprojectbackend.entity.Materials;
import cm.third.project.group.java20200120finalprojectbackend.entity.Resource;
import cm.third.project.group.java20200120finalprojectbackend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceRepository extends JpaRepository<Resource, Integer> {

    Resource findByUserAndMaterial(User user, Materials generatesMaterial);
}
