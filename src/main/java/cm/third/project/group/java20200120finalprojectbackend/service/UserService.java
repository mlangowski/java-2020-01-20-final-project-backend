package cm.third.project.group.java20200120finalprojectbackend.service;

import cm.third.project.group.java20200120finalprojectbackend.dto.UserDto;
import cm.third.project.group.java20200120finalprojectbackend.entity.*;
import cm.third.project.group.java20200120finalprojectbackend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final ResourceRepository resourceRepository;
    private final UnitRepository unitRepository;
    private final UserToDtoAdapter userToDtoAdapter;

    @Autowired
    public UserService(UserRepository userRepository, ResourceRepository resourceRepository,
                       UnitRepository unitRepository, UserToDtoAdapter userToDtoAdapter) {

        this.userRepository = userRepository;
        this.resourceRepository = resourceRepository;
        this.unitRepository = unitRepository;
        this.userToDtoAdapter = userToDtoAdapter;
    }

    public Boolean isAuth(Integer id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        Optional<User> authUser = userRepository.findByUserName(name);
        return authUser.filter(user -> Objects.equals(user.getId(), id)).isPresent();
    }

    public Optional<UserDto> getActualUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        Optional<User> authUser = userRepository.findByUserName(name);
        if (authUser.isPresent()) {
            UserDto userDto = userToDtoAdapter.getUserDto(authUser.get());
            return Optional.ofNullable(userDto);
        }
        return Optional.empty();
    }

    public boolean isAdmin() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName().equals("admin");
    }

    public void addUser(User user) {

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(12);

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRole("ROLE_USER");
        user.setEnabled(true);

        List<Unit> units = new ArrayList<>();
        List<UnitsProperties> unitsProperties = new ArrayList<>();
        List<Resource> resources = new ArrayList<>();

        for (int i = 0; i < Materials.values().length; i++) {
            resources.add(new Resource());
            resources.get(i).setMaterial(Materials.values()[i]);
            resources.get(i).setQuantity(5 - i);
        }

        for (int i = 0; i < Units.values().length; i++) {

            units.add(new Unit());
            unitsProperties.add(new UnitsProperties());
            unitsProperties.get(i).setId(Units.values()[i]);
            units.get(i).setUnitsProperties(unitsProperties.get(i));
        }

        user.setResources(resources);
        user.setUnits(units);
        userRepository.save(user);

        for (int i = 0; i < Units.values().length; i++) {

            units.get(i).setUser(user);
            resources.get(i).setUser(user);
            resourceRepository.save(resources.get(i));
            unitRepository.save(units.get(i));
        }
    }
}
